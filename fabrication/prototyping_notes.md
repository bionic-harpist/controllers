Prototyping notes
=================

## Control modules - 1st design

Based on a [drawing and touchOSC mockup][1], module 1 is a panel comprised of 4 "channel strips", with each column comprised of a selection of controls for an assigned track in an Ableton Live Set. ACtual layout will vary based on size and shape of panel placement

![module 1 CAD](../media/module_1_channel_strips_2019-Nov-12_01-12-59AM-000_CustomizedView16602331590_png_alpha.png)

[1]: ../media/20190925_touchOSC_layout.jpg

## Specs: 

- Wemos LOLIN D32 Pro
- LiPo battery power 
- 4051 multiplexers, one fore each group of 8 analog inputs
    - the D32 has 5 analog pins and several digital pins, so 40 analog inputs nd many digital inputs with multiplexing
- Transmit with BLE MIDI 
    - https://github.com/neilbags/arduino-esp32-BLE-MIDI
    - check this: https://www.youtube.com/watch?v=P0aqbD9umDE&feature=youtu.be
    - see also: https://learn.sparkfun.com/tutorials/midi-ble-tutorial/all
- Magnets will attach the module(s) to the harp

@todos: 

- NeoPixel integration (via bidirectional MIDI?)
    - see the [Adafruit NeoPixel guide](https://learn.adafruit.com/adafruit-neopixel-uberguide?view=all)
    - Need to develop a Max for Live patch to do some simple audio analysis and get basic track information, then transmit to control modules as MIDI messages for NeoPixel FX. 
    - Power: can use cell phone battery packs w/ USB connectors (have some from old BodySuit kits), or with big 3-cell LiPos

## Shopping list: 

- see [~/materials.md](../materials.md)
  
## Design notes: 

### Software: 

- did a prototype of an analog potentiometer with 4051 multiplexer transmitting via serial
    - https://playground.arduino.cc/Learning/4051/
- also demoed the BLE MIDI example (see above)
- next: combine them and scale up
    - 8 analog inputs > 4051 > BLE MIDI > Ableton Live MIDI input

### Hardware: 

- CAD: Control module 1, a generic layout based on our touchOSC controls (shown at top)
- CAD: working on the full soundboard panels with custom control layout
    - 1 module on each side
    - NeoPixel strips integrated on each panel

![physical prototype](../media/IMG_8170.jpg)
![physical prototype](../media/IMG_8156.jpg)

