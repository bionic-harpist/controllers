uint8_t trColor[16] = {   
  // trellis button colors if TOGGLE
  // using the Trellis Wheel() function
  // approx. color vals: 0 = green; 85 = red; 170 = blue
  85, 85, 85, 85,   
  85, 85, 85, 85,
  48, 160, 85, 213,
  85, 85, 85, 85
};


//define a callback for key presses
TrellisCallback trellisInput(keyEvent evt){
  // Check is the pad pressed?
  if (inTypeTr[evt.bit.NUM] == 0) { // momentary buttons
    if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
      trellis.pixels.setPixelColor(evt.bit.NUM, Wheel(map(evt.bit.NUM, 0, trellis.pixels.numPixels(), 0, 255))); //on rising
      valsTr[evt.bit.NUM] = 127; 
    } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    // or is the pad released?
      trellis.pixels.setPixelColor(evt.bit.NUM, 0); //off falling
      valsTr[evt.bit.NUM] = 0; 
    }
  } else if (inTypeTr[evt.bit.NUM] == 1) { // on/off buttons
    // latching buttons
    if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) { // if button down
      if (valsTr[evt.bit.NUM] == 0) { // and current value is 0
//        trellis.pixels.setPixelColor(evt.bit.NUM, 0xff0000); //on rising
        trellis.pixels.setPixelColor(evt.bit.NUM, Wheel(trColor[evt.bit.NUM])); //on rising
        valsTr[evt.bit.NUM] = 127;
      } else {
        trellis.pixels.setPixelColor(evt.bit.NUM, 0);
        valsTr[evt.bit.NUM] = 0;
      }
    }
    
  }

  // Turn on/off the neopixels!
  trellis.pixels.show();

  return 0;
}

// Input a value 0 to 255 to get a color value.
// The colors are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
   return trellis.pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return trellis.pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return trellis.pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  return 0;
}
