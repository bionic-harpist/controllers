This contains the Arduino sketches that run the controllers. 

The same sketch is used for either controller, but there is a `MODULE` variable at the top of the sketch that can be set `0` for left and `1` for right. This maps the correct inputs to the correct MIDI mappings. 

There are three versions of the software in this folder: 

1. `bionic_harp`: the original sketch
2. `bionic_harp_ewma_filter`: rewritten sketch with an Exponential Windowed Moving Average filter implemented for continuous control inputs (like potentiometers) to minimize MIDI messages flickering between 2 values
3. __*__`bionic_harp_running_average`: same as above, but with a basic running average filter implemented unstead of EWMA.

#3 `bionic_harp_running_average` is currently loaded on the controllers as it produced the best results in testing.