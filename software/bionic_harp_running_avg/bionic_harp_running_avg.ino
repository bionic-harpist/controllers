/******************************
 * The Bionic Harp
 * Modular BLE MIDI controllers 
 * augmenting concrt harp
 * 
 * for the Wemos Lolin D32 PRO

 * 
 * https://gitlab.com/bionic-harpist/controllers
 * John D. Sullivan 
 * 
 ******************************/

#include "Adafruit_NeoTrellis.h"
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

#define MODULE 0  // 0 == LEFT
                  // 1 == RIGHT

#define DELTIME 1 
bool DEBUG = false; // print output to serial

// BLE MIDI UUIDs
#define SERVICE_UUID        "03b80e5a-ede8-4b33-a751-6ce34ec4c700"
#define CHARACTERISTIC_UUID "7772e5db-3868-4112-a1a9-f2669d106bf3"

// Pin definitions

// input pins for the MUX boards: 0-3 == LH; 4 == RH
const uint8_t mux[4] = {34, 32, 33, 34}; 
#define LEDPIN 5

// digital select pins
#define S0 4
#define S1 0
#define S2 2

int r0 = 0; // value of select pin at the 4051 (s0)
int r1 = 0; // value of select pin at the 4051 (s1)
int r2 = 0; // value of select pin at the 4051 (s2)

uint8_t thisVal = 0;

long prevMillis = 0;
long currMillis = 0;
bool ledState = false;

// MIDI values to send
uint8_t valsAn[4][8] = { // analog inputs, MUX 0-3
  {0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0} };
uint8_t valsTr[16] = {   // trellis inputs
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// last input values
uint8_t lastValsAn[4][8] = { // analog inputs, MUX 0-3
  {0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},
  {0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0} };
uint8_t lastValsTr[16] = {   // trellis inputs
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// MIDI CC#s
uint8_t midiCcAn[4][8] = { // analog inputs, MUX 0-3
  // CC# 20 - 27 (MUX0) LH
  {0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b},
  // CC# 28 - 35 (MUX1) LH
  {0x1c,0x1d,0x1e,0x1f,0x20,0x21,0x22,0x23},
  // CC# 36 - 43 (MUX2) LH
  {0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b},
  // CC# 44 - 51 (MUX3) RH 
  {0x2c,0x2d,0x2e,0x2f,0x30,0x31,0x32,0x33} };
uint8_t midiCcTr[16] = {   // trellis inputs
  // CC# 52 - 67 (TRELLIS) RH
  0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,
  0x3c,0x3d,0x3e,0x3f,0x40,0x41,0x42,0x43};

// input type (0 = momentary button, 1 = on/off, 2 = continuous)
uint8_t inTypeAn[4][8] = { // analog inputs, MUX 0-3
  {1,1,1,1,1,1,1,1},{0,0,0,0,0,0,0,0},
  {1,1,1,1,2,2,2,2},{2,2,2,2,2,2,2,2} };
  //   {0,0,0,0,1,1,1,1},{0,0,0,0,0,0,0,0}, // proposed controller updates
  // {0,0,0,0,2,2,2,2},{2,2,2,2,2,2,2,2} };
uint8_t inTypeTr[16] = {   // trellis inputs
  0,0,1,1,0,0,0,0,1,1,1,1,0,0,0,0};

uint8_t numReadings = 75; // if you change this, you have to change the line below too
uint16_t readings[4][8][75];
int totals[4][8];

Adafruit_NeoTrellis trellis; // create a Trellis object

// *********** BLE-MIDI initialization **********//

#define SERVICE_UUID        "03b80e5a-ede8-4b33-a751-6ce34ec4c700"
#define CHARACTERISTIC_UUID "7772e5db-3868-4112-a1a9-f2669d106bf3"

BLECharacteristic *pCharacteristic;
bool deviceConnected = false;

uint8_t midiPacket[] = {
  0x80, // header
  0x80, // timestamp, not implemented
  0xB0, // status byte (Continuous controller)
  0x00, // data byte 1 (Controller #)
  0x00, // data byte 2 (Controller value)
};

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.println("BLE device connected :) "); 
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.println("BLE device disconnected :( ");
    }
};

void setup() {
  Serial.begin(115200);
  
  if(MODULE == 0) {
    BLEDevice::init("Bionic_Harp_LH");
  } else {
    BLEDevice::init("Bionic_Harp_RH");
  }
    
  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(BLEUUID(SERVICE_UUID));

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
    BLEUUID(CHARACTERISTIC_UUID),
    BLECharacteristic::PROPERTY_READ   |
    BLECharacteristic::PROPERTY_WRITE  |
    BLECharacteristic::PROPERTY_NOTIFY |
    BLECharacteristic::PROPERTY_WRITE_NR
  );

  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->addServiceUUID(pService->getUUID());
  pAdvertising->start();

  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);

  pinMode(LEDPIN, OUTPUT);

  // input pins for the MUX boards (0-3 LH; 4 RH)
  for(uint8_t i=0;i<4;i++) { 
    pinMode(mux[i], INPUT);
  }
  
  if (MODULE == 1) {
    if (!trellis.begin()) {
      Serial.println("Could not start trellis, check wiring?");
      while(1);
    } else {
      Serial.println("NeoPixel Trellis started");
    }
  
    //activate all keys and set callbacks
    for(uint8_t i=0; i<NEO_TRELLIS_NUM_KEYS; i++){
      trellis.activateKey(i, SEESAW_KEYPAD_EDGE_RISING);
      trellis.activateKey(i, SEESAW_KEYPAD_EDGE_FALLING);
      trellis.registerCallback(i, trellisInput);
    }
  
    //do a little animation to show we're on
    for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
      trellis.pixels.setPixelColor(i, Wheel(map(i, 0, trellis.pixels.numPixels(), 0, 255)));
      trellis.pixels.show();
      delay(50);
    }
    for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
      trellis.pixels.setPixelColor(i, 0x000000);
      trellis.pixels.show();
      delay(50);
    }
  }

  // initialize variables for pot smoothing  
  for(uint8_t i=0;i<4;i++) {
    for(uint8_t j=0;j<8;j++) {
      totals[i][j] = 0;
      for(uint8_t k=0;k<numReadings;k++) {
        readings[i][j][k] = 0;
      }
    }
  }
  
  Serial.println("The Bionic Harp"); 
  if (MODULE == 0) {
    Serial.println("LEFT module");
  } else {
    Serial.println("RIGHT module");
  }
  for(uint8_t i=0; i<10; i++){
    digitalWrite(LEDPIN, HIGH);
    Serial.print(". ");
    delay(100);
    digitalWrite(LEDPIN, LOW);
    delay(100);
  }
  Serial.println("");
}

void loop() {
  
  // just blink the light to know we're alive...
  currMillis = millis();
  if ( currMillis > prevMillis + 667) {
    if (ledState) {
      digitalWrite(LEDPIN, LOW);
      ledState = false;
    } else {
      digitalWrite(LEDPIN, HIGH);
      ledState = true;
    }
    prevMillis = currMillis;
  }

  if (MODULE == 0) { // LH MODULE
    readMux(0); 
    readMux(1); 
    readMux(2); 
  } else { // RH MODULE
    readMux(3);
    readTrellis();
  }

  delay(DELTIME);
}

void readMux(uint8_t mx) {
  for(uint8_t y=0; y<8; y++) {
    
    r0 = bitRead(y, 0);
    r1 = bitRead(y, 1);
    r2 = bitRead(y, 2);

    digitalWrite(S0, r0);
    digitalWrite(S1, r1);
    digitalWrite(S2, r2);

    thisVal = translate(analogRead(mux[mx]), inTypeAn[mx][y], mx, y); 
    if(thisVal != lastValsAn[mx][y]) {
      switch(inTypeAn[mx][y]) {
        case 0: // new momentary button state (0 or 127)
          valsAn[mx][y] = thisVal;
          break;
        case 1: // toggle - only fire if button val is 127
          if(thisVal == 127) {
            if(valsAn[mx][y] == 0) {
              valsAn[mx][y] = 127;
            } else {
              valsAn[mx][y] = 0;
            }
          }
          break;
        case 2: // continuous control (0 to 127)
          valsAn[mx][y] = thisVal;
          break;       
      }
//      if(mx==1 && thisVal!=127) {
      if(inTypeAn[mx][y]==1 && thisVal!=127) {
        // do nothing 
      } else {
        sendMIDImessage(midiCcAn[mx][y], valsAn[mx][y]);
        if(DEBUG) {
          Serial.print("MUX");
          Serial.print(mx);
          Serial.print("-"); 
          Serial.print(y); 
          Serial.print(": "); 
          Serial.println(valsAn[mx][y]);   
        }
      }
      lastValsAn[mx][y] = thisVal; 
    }   
  }
}

void readTrellis() {
  trellis.read(); // get data from the trellis
  for(uint8_t z=0; z<16; z++) { // cycle through all the buttons
    if(valsTr[z] != lastValsTr[z]) {
      sendMIDImessage(midiCcTr[z], valsTr[z]);
      lastValsTr[z] = valsTr[z];

      if(DEBUG) {
        Serial.print("TREL-"); 
        Serial.print(z); 
        Serial.print(": "); 
        Serial.println(valsTr[z]);
      }
    }
  }
}

uint8_t translate(uint16_t val, uint8_t type, uint8_t mux, uint8_t ct) {
  // inputs: 
  //    val:      input value (0 - 4095)
  //    type:     0 (momentary) / 1 (toggle) / 2 (continuous)
  //    mux       0 - 3 multiplexers
  //    ct        0 - 7 which input
  //    lastVal:  last output val (check for change for type 1)

  uint8_t translatedValue;
  
  switch(type) {
    case 0: 
      if(val >=2047) {
        translatedValue = 127;
      } else {
        translatedValue = 0;
      }
      return translatedValue; 
    case 1: 
      if(val >=2047) {
        translatedValue = 127;
      } else {
        translatedValue = 0;
      }
      return translatedValue;
    case 2: 
      // take average of last n readings

      // running total: subtract oldest (n-1) and add incoming
      totals[mux][ct] = totals[mux][ct] - readings[mux][ct][numReadings-1] + val;
      
      uint16_t average = totals[mux][ct]/numReadings; // get the average of last n readings

      for(uint8_t i=numReadings-1;i>0;i--) { // count down numReadings-1 to 0;
        readings[mux][ct][i] = readings[mux][ct][i-1]; // move readings over a place
      }
      readings[mux][ct][0] = val; // add incoming val to 1st position in array

      // translate value to MIDI 0-127
      translatedValue = uint8_t(round(map(average, 0, 4095, 0, 127)));
//      Serial.println(translatedValue);
      return translatedValue;
  }
}

void sendMIDImessage (uint8_t cc, uint8_t val) {
  if (deviceConnected) {
    midiPacket[3] = cc;
    midiPacket[4] = val;

    pCharacteristic->setValue(midiPacket, 5); // packet, length in bytes
    pCharacteristic->notify();
    if(DEBUG) {
      Serial.print("Sending MIDI: ");
      for(int i=0;i<3;i++) {
        Serial.print(midiPacket[i+2]); 
        Serial.print(" "); 
      }
      Serial.println(); 
    }
  } else {
    Serial.println("BLE not connected!!");
  }
}
